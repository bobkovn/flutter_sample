import 'package:flutter_todo_list/model/task.dart';

import 'database/dbprovider.dart';

class Repository {
  final DBProvider _database = DBProvider();

  Repository();

  Future<int> saveTask(Task task) async {
    return await _database.insertTask(task);
  }

  Future<List<Task>> getUncompletedTasks() async {
    return await _database.getTasks(false);
  }

  Future<List<Task>> getCompletedTasks() async {
    return await _database.getTasks(true);
  }

  Future updateTask(Task task) async {
    await _database.updateTask(task);
  }

  Future deleteTask(int id) async {
    await _database.deleteTask(id);
  }
}
