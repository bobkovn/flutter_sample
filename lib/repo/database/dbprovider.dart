import 'dart:io';

import 'package:flutter_todo_list/model/task.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';

class DBProvider {
  DBProvider();

  Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "TaskDB.db");
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE Task ("
          "id INTEGER PRIMARY KEY AUTOINCREMENT,"
          "title TEXT,"
          "description TEXT,"
          "time INTEGER,"
          "completedAt INTEGER,"
          "isCompleted BIT"
          ")");
    });
  }

  Future insertTask(Task task) async {
    final db = await database;
    return await db.rawInsert(
        "INSERT Into Task (title,description,time,completedAt,isCompleted)"
        " VALUES (?,?,?,?,?)",
        [
          task.title,
          task.description,
          task.time,
          task.completedAt,
          task.isCompleted
        ]);
  }

  Future<List<Task>> getTasks(bool isCompleted) async {
    final db = await database;
    var res = await db.query("Task",
        where: "isCompleted = ?", whereArgs: [isCompleted ? 1 : 0]);
    List<Task> list =
        res.isNotEmpty ? res.map((c) => Task.fromMap(c)).toList() : [];
    return list;
  }

  updateTask(Task task) async {
    final db = await database;
    var res = await db
        .update("Task", task.toMap(), where: "id = ?", whereArgs: [task.id]);
    return res;
  }

  deleteTask(int id) async {
    final db = await database;
    db.delete("Task", where: "id = ?", whereArgs: [id]);
  }
}
