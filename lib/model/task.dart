class Task {
  final int id;
  final String title;
  final String description;
  final int time;
  final int completedAt;
  final bool isCompleted;

  Task(
      {this.id,
      this.title,
      this.description,
      this.time,
      this.completedAt,
      this.isCompleted});

  Map<String, dynamic> toMap() => {
        'id': id,
        'title': title,
        'description': description,
        'time': time,
        'completedAt': completedAt,
        'isCompleted': isCompleted,
      };

  factory Task.fromMap(dynamic map) {
    var isCompleted = map['isCompleted'] as int == 1;
    return Task(
        id: map['id'] as int,
        title: map['title'] as String,
        description: map['description'] as String,
        time: map['time'] as int,
        completedAt: map['completedAt'] as int,
        isCompleted: isCompleted);
  }

  Task cloneWith(
      {final int id,
      final String title,
      final String description,
      final int time,
      final int completedAt,
      final bool isCompleted}) {
    return Task(
        id: id ?? this.id,
        title: title ?? this.title,
        description: description ?? this.description,
        time: time ?? this.time,
        completedAt: completedAt ?? this.completedAt,
        isCompleted: isCompleted ?? this.isCompleted);
  }
}
