import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_todo_list/bloc/basetaskbloc.dart';
import 'package:flutter_todo_list/model/task.dart';

const double _PADDING_CONTAINER = 12;
const double _CIRCULAR_RADIUS = 15;
const double _PADDING_TOP = 16;
const double _FONT_SIZE = 20;
const double _BUTTON_WIDTH = 320;
class CreateTaskDialog extends StatelessWidget {
  final BaseTaskBloc _bloc;
  final _taskTitleController = TextEditingController();
  final _taskDescriptionController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  CreateTaskDialog(this._bloc);

  @override
  Widget build(BuildContext context) {
    return Dialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(_CIRCULAR_RADIUS)),
        child: Wrap(children: [
          Container(
            child: Padding(
              padding: const EdgeInsets.all(_PADDING_CONTAINER),
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Create task:",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: _FONT_SIZE)),
                    TextFormField(
                      keyboardType: TextInputType.multiline,
                      maxLines: null,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                      controller: _taskTitleController,
                      decoration: InputDecoration(hintText: 'Enter title'),
                    ),
                    TextFormField(
                      keyboardType: TextInputType.multiline,
                      maxLines: null,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                      controller: _taskDescriptionController,
                      decoration:
                          InputDecoration(hintText: 'Enter description'),
                    ),
                    Padding(
                        padding: const EdgeInsets.only(top: _PADDING_TOP),
                        child: SizedBox(
                          width: _BUTTON_WIDTH,
                          child: RaisedButton(
                            onPressed: () {
                              if (_formKey.currentState.validate()) {
                                _saveTask(context);
                              } else {
                                Scaffold.of(context).showSnackBar(SnackBar(
                                    content: Text('Please enter some text')));
                              }
                            },
                            child: Text(
                              "Save",
                              style: TextStyle(color: Colors.white),
                            ),
                            color: Colors.blue,
                          ),
                        ))
                  ],
                ),
              ),
            ),
          ),
        ]));
  }

  void _saveTask(BuildContext context) {
    _bloc.saveTask(Task(
        title: _taskTitleController.text,
        description: _taskDescriptionController.text,
        time: DateTime.now().millisecondsSinceEpoch,
        isCompleted: false));
    _taskTitleController.clear();
    _taskDescriptionController.clear();
    Navigator.of(context).pop();
  }
}
