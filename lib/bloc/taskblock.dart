import 'package:flutter_todo_list/bloc/basetaskbloc.dart';
import 'package:flutter_todo_list/model/task.dart';
import 'package:flutter_todo_list/repo/repository.dart';

class TaskBloc extends BaseTaskBloc {
  TaskBloc(Repository repository) : super(repository);

  @override
  Future<List<Task>> fetchTasks() {
    return repository.getUncompletedTasks();
  }
}
