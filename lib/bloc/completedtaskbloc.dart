import 'package:flutter_todo_list/model/task.dart';
import 'package:flutter_todo_list/repo/repository.dart';
import 'basetaskbloc.dart';

class CompletedTaskBloc extends BaseTaskBloc {
  CompletedTaskBloc(Repository repository) : super(repository);

  @override
  Future<List<Task>> fetchTasks() {
    return repository.getCompletedTasks();
  }
}
