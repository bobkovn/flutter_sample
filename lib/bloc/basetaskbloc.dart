import 'dart:async';

import 'package:flutter_todo_list/model/task.dart';
import 'package:flutter_todo_list/repo/repository.dart';

abstract class BaseTaskBloc {
  BaseTaskBloc(this.repository);

  final Repository repository;

  final StreamController _streamController =
      StreamController<List<Task>>.broadcast();

  final List<Task> _tasks = List<Task>();

  Stream<List<Task>> get data => _streamController.stream;

  Future obtainTasks() async {
    _tasks.clear();
    _tasks.addAll(await fetchTasks());
    _streamController.sink.add(_tasks);
  }

  Future<List<Task>> fetchTasks();

  Future saveTask(Task task) async {
    int id = await repository.saveTask(task);
    _addTask(task.cloneWith(id: id));
  }

  Future toggleComplete(Task task) async {
    int completedAt = 0;
    bool isCompleted = !task.isCompleted;
    if (isCompleted) {
      completedAt = DateTime.now().millisecondsSinceEpoch;
    }
    await repository.updateTask(
        task.cloneWith(completedAt: completedAt, isCompleted: isCompleted));
    _removeTask(task.id);
  }

  Future updateTask(Task task) async {
    await repository.updateTask(task);
  }

  Future deleteTask(Task task) async {
    await repository.deleteTask(task.id);
  }

  void _addTask(Task task) {
    _tasks.add(task);
    notifyChange();
  }

  void _removeTask(int id) {
    _tasks.removeWhere((item) => item.id == id);
    notifyChange();
  }

  void notifyChange() {
    _streamController.sink.add(_tasks);
  }

  void dispose() {
    _streamController.close();
  }
}
