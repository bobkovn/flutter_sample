import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_todo_list/bloc/basetaskbloc.dart';
import 'package:flutter_todo_list/model/task.dart';
import 'package:intl/intl.dart';

const double _PADDING = 16;
const double _PADDING_TRASH_ICON = 20;
const double _FONT_SIZE = 20;
const double _BUTTON_WIDTH = 320;

class DetailTaskScreen extends StatelessWidget {
  final Task _task;
  final BaseTaskBloc _bloc;
  final _taskTitleController = TextEditingController();
  final _taskDescriptionController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  DetailTaskScreen(this._task, this._bloc);

  @override
  Widget build(BuildContext context) {
    _taskTitleController.text = _task.title;
    _taskDescriptionController.text = _task.description;
    return Scaffold(
      appBar: AppBar(
        title: Text(_task.title),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: _PADDING_TRASH_ICON),
            child: GestureDetector(
              onTap: () {
                _deleteTask(context);
              },
              child: Icon(
                Icons.delete_forever, // add custom icons also
              ),
            ),
          ),
        ],
      ),
      body: Container(
          child: Padding(
        padding: const EdgeInsets.all(_PADDING),
        child: Column(
          children: <Widget>[
            Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Update task:",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: _FONT_SIZE)),
                  TextFormField(
                    keyboardType: TextInputType.multiline,
                    maxLines: null,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter some text';
                      }
                      return null;
                    },
                    controller: _taskTitleController,
                    decoration: InputDecoration(hintText: 'Update title'),
                  ),
                  TextFormField(
                    keyboardType: TextInputType.multiline,
                    maxLines: null,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter some text';
                      }
                      return null;
                    },
                    controller: _taskDescriptionController,
                    decoration: InputDecoration(hintText: 'Update description'),
                  ),
                ],
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                CompletedTime(_task.completedAt),
                Padding(
                    padding: const EdgeInsets.only(top: _PADDING),
                    child: SizedBox(
                      width: _BUTTON_WIDTH,
                      child: RaisedButton(
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            _updateTask(context);
                          } else {
                            Scaffold.of(context).showSnackBar(SnackBar(
                                content: Text('Please enter some text')));
                          }
                        },
                        child: Text(
                          "Update",
                          style: TextStyle(color: Colors.white),
                        ),
                        color: Colors.blue,
                      ),
                    )),
              ],
            )
          ],
        ),
      )),
    );
  }

  void _updateTask(BuildContext context) {
    _bloc.updateTask(_task.cloneWith(
        title: _taskTitleController.text,
        description: _taskDescriptionController.text));
    Navigator.of(context).pop();
  }

  void _deleteTask(BuildContext context) {
    _bloc.deleteTask(_task);
    Navigator.of(context).pop();
  }
}

class CompletedTime extends StatelessWidget {
  final int _time;
  final _formatter = DateFormat('dd/MM/yyyy HH:mm a');

  CompletedTime(this._time);

  @override
  Widget build(BuildContext context) {
    String completedDate = "You have not completed task";
    if (_time != null && _time != 0) {
      var date = _formatter.format(DateTime.fromMillisecondsSinceEpoch(_time));
      completedDate = "Completed time: $date";
    }
    return Padding(
      padding: const EdgeInsets.only(top: _PADDING),
      child: Text(completedDate),
    );
  }
}
