import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_todo_list/bloc/basetaskbloc.dart';
import 'package:flutter_todo_list/bloc/completedtaskbloc.dart';
import 'package:flutter_todo_list/bloc/taskblock.dart';
import 'package:flutter_todo_list/model/task.dart';
import 'package:flutter_todo_list/repo/repository.dart';
import 'package:flutter_todo_list/widgets/createtaskdialog.dart';
import 'package:intl/intl.dart';

import 'detailsscreen.dart';

const double _PADDING = 16;
const double _FONT_SIZE_HEADER = 16;
const double _FONT_SIZE_TITLE = 20;
const double _FONT_SIZE_SUBTITLE = 18;
const double _FONT_SIZE_TIME = 14;
const int _MAX_LINES = 1;
const int _TAB_COUNT = 2;

void main() => runApp(ToDoApp());

class ToDoApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '//TODO:',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: TabBarTasks(),
    );
  }
}

class TabBarTasks extends StatefulWidget {
  @override
  _TabBarTasksState createState() => _TabBarTasksState();
}

class _TabBarTasksState extends State<TabBarTasks> {
  BaseTaskBloc _taskBloc;
  BaseTaskBloc _completedTaskBloc;

  @override
  void initState() {
    super.initState();
    Repository repository = Repository();
    _taskBloc = TaskBloc(repository);
    _completedTaskBloc = CompletedTaskBloc(repository);
  }

  @override
  void dispose() {
    _taskBloc.dispose();
    _completedTaskBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: _TAB_COUNT,
      child: Scaffold(
          appBar: AppBar(
            bottom: TabBar(
              tabs: [
                Tab(text: "TO DO"),
                Tab(text: "COMPLETED"),
              ],
            ),
            title: Text('//TODO:'),
          ),
          body: TabBarView(
            children: [
              TasksList(_taskBloc),
              TasksList(_completedTaskBloc),
            ],
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return CreateTaskDialog(_taskBloc);
                  });
            },
            tooltip: 'Add new task',
            child: Icon(Icons.add),
          )),
    );
  }
}

class TasksList extends StatefulWidget {
  final BaseTaskBloc _bloc;

  TasksList(this._bloc);

  @override
  _TasksListState createState() => _TasksListState();
}

class _TasksListState extends State<TasksList> {
  final _headerFont = const TextStyle(
      fontSize: _FONT_SIZE_HEADER,
      color: Colors.black);
  final _titleFont = const TextStyle(
      fontSize: _FONT_SIZE_TITLE,
      color: Colors.black,
      fontWeight: FontWeight.bold);
  final _subtitleFont = const TextStyle(fontSize: _FONT_SIZE_SUBTITLE);
  final _timeFont =
  const TextStyle(fontSize: _FONT_SIZE_TIME, color: Colors.grey);
  final _formatter = DateFormat('dd/MM/yyyy HH:mm a');

  @override
  Widget build(BuildContext context) {
    widget._bloc.obtainTasks();
    return StreamBuilder<List<Task>>(
        stream: widget._bloc.data,
        builder: (context, AsyncSnapshot<List<Task>> snapshot) {
          List<Task> tasks = snapshot.data;
          if (tasks != null && tasks.isNotEmpty) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: _PADDING, top: _PADDING),
                  child: Text("Count: ${tasks.length}", style: _headerFont),
                ),
                Expanded(child: _buildTasksList(tasks)),
              ],
            );
          } else {
            return Center(
                child: Text(
              "Empty",
              style: TextStyle(
                  fontWeight: FontWeight.bold, fontSize: _FONT_SIZE_TITLE),
            ));
          }
        });
  }

  Widget _buildTasksList(List<Task> data) {
    return ListView.builder(
        padding: const EdgeInsets.all(_PADDING),
        itemCount: data.length,
        itemBuilder: (context, i) {
          return _buildRow(data[i]);
        });
  }

  Widget _buildRow(Task task) {
    var date = _formatter.format(DateTime.fromMillisecondsSinceEpoch(task.time));
    return Dismissible(
      key: Key(task.id.toString()),
      onDismissed: (direction) {
        widget._bloc.deleteTask(task);
      },
      child: InkWell(
        onTap: () {
          _showDetailScreen(task);
        },
        child: Column(
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        task.title,
                        style: _titleFont,
                        overflow: TextOverflow.ellipsis,
                        maxLines: _MAX_LINES,
                      ),
                      Text(
                        task.description,
                        style: _subtitleFont,
                        overflow: TextOverflow.ellipsis,
                        maxLines: _MAX_LINES,
                      ),
                      Text(
                        date,
                        style: _timeFont,
                        overflow: TextOverflow.ellipsis,
                        maxLines: _MAX_LINES,
                      )
                    ],
                  ),
                ),
                Expanded(
                  flex: 0,
                  child: IconButton(
                    icon: Icon(
                      task.isCompleted
                          ? Icons.check_box
                          : Icons.check_box_outline_blank,
                      color: task.isCompleted ? Colors.red : null,
                    ),
                    onPressed: () {
                      widget._bloc.toggleComplete(task);
                    },
                  ),
                ),
              ],
            ),
            Divider()
          ],
        ),
      ),
    );
  }

  void _showDetailScreen(Task task) {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (BuildContext context) {
          return DetailTaskScreen(task, widget._bloc);
        },
      ),
    );
  }
}
